'use strict';
require('es6-promise').polyfill();

var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');

var sassConfig = {outpuStyle: 'compressed'}

gulp.task('sass', function(){
    return gulp.src('./public/sass/**/*.scss')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(sass(sassConfig).on('error', sass.logError))

        .pipe(gulp.dest('./public/css'));
});

gulp.task('sass:watch', function (){
    gulp.watch('./public/sass/**/*.scss', ['sass']);
});