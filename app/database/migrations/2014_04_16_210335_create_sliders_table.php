<?php

use Illuminate\Database\Migrations\Migration;

class CreateSlidersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('slides')){
			Schema::create('slides', function(Blueprint $table) {
				$table->increments('id');
				$table->string('filename', 64);
				$table->string('url', 256);
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}