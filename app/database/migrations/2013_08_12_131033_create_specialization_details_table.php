<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSpecializationDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('specialization_details', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('id_specialization');
			$table->text('about');
			$table->text('recruitment');
			$table->text('teaching_plan');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('specialization_details');
	}

}
