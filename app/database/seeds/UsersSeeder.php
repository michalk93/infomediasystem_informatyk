<?php

class UsersSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('usersseeder')->truncate();

		$usersseeder = array(
			'email'			=> 'bncdn@poczta.fm',
			'password'		=> Hash::make('admin'),
			'created_at'	=> date('Y-m-d'),
		);

		User::create($usersseeder);
		// Uncomment the below to run the seeder
		// DB::table('usersseeder')->insert($usersseeder);
	}

}
