<?php

/**
* School table seeder
*/
class SchoolSeeder extends Seeder
{
	/**
	* Run seed
	*/
	public function run()
	{
		School::create(array(
			'name' => 'Liceum Ogólnokształcące',
			'description' => 'Liceum to propozycja dla osób, które chcą zdobyć średnie wykształcenie nie rezygnując z pracy i codziennych obowiązków. Wiemy jak istotną role odgrywa dzisiaj ukończenie szkoły średniej i świadectwo dojrzałości.'
		));
		School::create(array(
			'name' => 'Studium',
			'description' => 'Szkoły policealne to dobre rozwiązanie dla tych, którzy nie dostali się na studia, a nie chcą tracić roku oraz dla osób, które nie zdały matury, a chcą się kształcić dalej. Okres kształcenia trwa 2 lata dla wszystkich kierunków.'
		));
		School::create(array(
			'name' => 'Studium Medyczne',
			'description' => 'Szkoły policealne to dobre rozwiązanie dla tych, którzy nie dostali się na studia, a nie chcą tracić roku oraz dla osób, które nie zdały matury, a chcą się kształcić dalej. Okres kształcenia wynosi 1 rok lub 2 lata w zależności od kierunku.'
		));
	}
}