<?php



/*
|--------------------------------------------------------------------------
| ADMINISTRATION SITE ROUTING
|--------------------------------------------------------------------------
*/

App::missing(function($exception) {
    return Redirect::route('home');
});

Route::group(array('prefix' => 'admin'), function(){

	// Administration login page or start page when user is logged
	Route::get('login', ['as' => 'admin-login', 'uses' => 'UserController@getLogin']);
	Route::post('login', ['uses' => 'UserController@postLogin']);

	Route::group(array('before' => 'auth'), function(){
		Route::get('/', ['as' => 'admin-home', 'uses' => 'HomeController@indexAdmin']);
		Route::get('logout', ['as' => 'admin-logout', 'uses' => 'UserController@logout']);

		Route::get('news', ['as' => 'admin-news', 'uses' => 'NewsController@indexAdmin']);
		Route::get('specialization', ['as' => 'admin-specialization', 'uses' => 'SpecializationController@indexAdmin']);
		Route::get('files', ['as' => 'admin-files', 'uses' => 'FileController@indexAdmin']);
		Route::get('filegroup', ['as' => 'admin-filegroup' , 'uses' => 'FilegroupController@indexAdmin']);
		Route::get('slides', ['as' => 'admin-slides', 'uses' => 'SliderController@indexAdmin']);

		Route::get('files/download/{filepath}', ['as' => 'admin-filedownload', 'uses' => 'FileController@getDownload']);

		Route::get('change-password/{id}', ['as' => 'admin-change-password', 'uses' => 'UserController@getChangePassword']);
		Route::post('change-password/{id}', ['as' => 'admin-change-password-post', 'uses' => 'UserController@postChangePassword']);

		// Adding new, createing routes
		Route::group(['prefix' => 'new'], function(){
			// New news
			Route::get('news', ['as' => 'admin-newnews', 'uses' => 'NewsController@getNew']);
			Route::post('news', ['as' => 'admin-newnews-post', 'uses' => 'NewsController@postNew']);

			// New specialization
			Route::get('speciaization', ['as' => 'admin-newspecialization', 'uses' => 'SpecializationController@getNew']);
			Route::post('speciaization', ['as' => 'admin-newspecialization-post', 'uses' => 'SpecializationController@postNew']);

			// Add file
			Route::get('file', ['as' => 'admin-newfile', 'uses' => 'FileController@getNew']);
			Route::post('file', ['as' => 'admin-newfile-post', 'uses' => 'FileController@postNew']);

			// Add filegroup
			Route::get('filegroup', ['as' => 'admin-newfilegroup', 'uses' => 'FilegroupController@getNew']);
			Route::post('filegroup', ['as' => 'admin-newfilegroup-post', 'uses' => 'FilegroupController@postNew']);

			// Add slider
			Route::get('slides', ['as' => 'admin-newslide', 'uses' => 'SliderController@getNew']);
			Route::post('slides', ['as' => 'admin-newslide-post', 'uses' => 'SliderController@postNew']);
		});

		// Editing existing elements
		Route::group(['prefix' => 'edit'], function(){
			// Edit news
			Route::get('news/{id}', ['as' => 'admin-editnews', 'uses' => 'NewsController@getEdit']);
			Route::post('news/{id}', ['as' => 'admin-editnews-post', 'uses' => 'NewsController@postEdit']);

			// Edit specialization
			Route::get('speciaization/{id}', ['as' => 'admin-editspecialization', 'uses' => 'SpecializationController@getEdit']);
			Route::post('speciaization', ['as' => 'admin-editspecialization-post', 'uses' => 'SpecializationController@postEdit']);

			// Edit filegroup
			Route::get('filegroup/{id}', ['as' => 'admin-editfilegroup', 'uses' => 'FilegroupController@getEdit']);
			Route::post('filegroup', ['as' => 'admin-editfilegroup-post', 'uses' => 'FilegroupController@postEdit']);

			//Edit existing file
			Route::get('file/{id}', ['as' => 'admin-editfile', 'uses' => 'FileController@getEdit']);
			Route::post('file', ['as' => 'admin-editfile-post', 'uses' => 'FileController@postEdit']);

			// Edit existing slide file
			Route::get('slide/{id}', ['as' => 'admin-editslide', 'uses' => 'SliderController@getEdit']);
			Route::post('slide', ['as' => 'admin-editslide-post', 'uses' => 'SliderController@postEdit']);
		});

		// Deleting existing elements
		Route::group(['prefix' => 'delete'], function(){
			// Delete news
			Route::get('news/{id}', ['as' => 'admin-deletenews', 'uses' => 'NewsController@getDelete']);

			// Delete specialization
			Route::get('speciaization/{id}', ['as' => 'admin-deletespecialization', 'uses' => 'SpecializationController@getDelete']);

			// Delete file
			Route::get('file/{id}', ['as' => 'admin-deletefile', 'uses' => 'FileController@getDelete']);

			// Delete filegroup
			Route::get('filegroup/{id}', ['as' => 'admin-deletefilegroup', 'uses' => 'FilegroupController@getDelete']);

			// Delete slide
			Route::get('slide/{id}', ['as' => 'admin-deleteslide', 'uses' => 'SliderController@getDelete']);			
		});
		
	});

	/*
	|--------------------------------------------------------------------------
	| ARTISAN COMMANDS ROUTING
	|--------------------------------------------------------------------------
	*/
	Route::group(array('prefix' => 'artisan'), function(){
		Route::get('migrate/{isSeeded?}', 'ArtisanController@migrate');
	});
});



/*
|--------------------------------------------------------------------------
| PUBLIC SITE ROUTING
|--------------------------------------------------------------------------
*/

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('contact', ['as' => 'contact', 'uses' => 'HomeController@showContact']);
Route::get('download/file/{id}', ['as' => 'downloadfile', 'uses' => 'FileController@getDownload']);
Route::get('elearning', ['as' => 'elearning', 'uses' => 'HomeController@getElearningPlatform']);


Route::group(['prefix' => 'school/{id}'], function(){
	Route::get('/', ['as' => 'school', 'uses' => 'HomeController@showSchool']);
});

Route::group(['prefix' => 'specialization/{id}'], function(){
	Route::get('/', ['as' => 'specialization', 'uses' => 'SpecializationController@index']);
});

Route::group(['prefix' => 'news/{id}'], function(){
	Route::get('/', ['as' => 'news', 'uses' => 'NewsController@index']);
});