<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| such as the size rules. Feel free to tweak each of these messages.
	|
	*/

	//"accepted"         => "The :attribute must be accepted.",
	//"active_url"       => "The :attribute is not a valid URL.",
	//"after"            => "The :attribute must be a date after :date.",
	"alpha"            => "Pole może zawierać tylko litery a-z/A-Z",
	"alpha_dash"       => "Pole może zawierząc tylko litery, cyfry i znak podkreślenia",
	"alpha_num"        => "Pole może zawierać tylko litery i cyfry",
	"before"           => "Pole musi zawierać datą przed :date.",
	"between"          => array(
		"numeric" => "Pole musi zawierać warotść od :min do :max",
		"file"    => "Niepoprawny rozmiar pliku. Plik musi mieć rozmiar od :min do :max KB",
		"string"  => "Pole musi zawierać od :min do :max znaków",
	),
	"confirmed"        => "Pole nie zostało pomyślnie potwierdzone",
	"date"             => "Niepoprawna data",
	"date_format"      => "The :attribute does not match the format :format.",
	"different"        => "The :attribute and :other must be different.",
	"digits"           => "The :attribute must be :digits digits.",
	"digits_between"   => "The :attribute must be between :min and :max digits.",
	"email"            => "Niepoprawny adres e-mail",
	"exists"           => "Wybrana wartość jest niepoprawna",
	"image"            => "The :attribute must be an image.",
	"in"               => "The selected :attribute is invalid.",
	"integer"          => "Pole może zawierać tylko liczby całkowite",
	"ip"               => "The :attribute must be a valid IP address.",
	"max"              => array(
		"numeric" => "Za duża wartość. Maksymalna wartość to :max",
		"file"    => "Za duży rozmiar pliku. Maksymalny rozmiar pliku to :max KB",
		"string"  => "Za dużo znaków. Maksymalna ilość znaków to :max",
	),
	"mimes"            => "The :attribute must be a file of type: :values.",
	"min"              => array(
		"numeric" => "Za mała wartość. Minimalna wartość to :min",
		"file"    => "Za mały rozmiar pliku. Minimalny rozmiar pliku to :min KB",
		"string"  => "Za mało znaków. Minimalna ilość znaków to :min",
	),
	"not_in"           => "The selected :attribute is invalid.",
	"numeric"          => "Pole musi zawierać liczbę",
	"regex"            => "Podany format jest niepoprawny.",
	"required"         => "Pole jest wymagane",
	"required_if"      => "The :attribute field is required when :other is :value.",
	"required_with"    => "The :attribute field is required when :values is present.",
	"required_without" => "The :attribute field is required when :values is not present.",
	"same"             => "The :attribute and :other must match.",
	"size"             => array(
		"numeric" => "The :attribute must be :size.",
		"file"    => "The :attribute must be :size kilobytes.",
		"string"  => "The :attribute must be :size characters.",
	),
	"unique"           => "Podana wartość już istnieje",
	"url"              => "The :attribute format is invalid.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(),

);
