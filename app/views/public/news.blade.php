@extends('public.template')

@section('title')
<title>{{strip_tags(Str::words($oNews->content, 5))}} - Beskidzka Szkoła Zawodowa</title>
@stop

@section('description')
<meta name="description" content="Aktualności {{$oNews->school_name}} - {{strip_tags(Str::words($oNews->content, 10))}}">
@stop

@section('keywords')
<meta name="keywords" content="aktualności {{$oNews->school_name}}, informacje {{$oNews->school_name}} sucha beskidzka, {{$oNews->school_name}} sucha beskidzka">
@stop

@section('content')

<div class="row white-box" style="padding: 20px">
	<div class="row">
		<div class="col-lg-12">
			<ul class="breadcrumb">
			    <li><a href="{{route('home')}}">Home</a></li>
			    <li><a href="{{route('school', $oNews->id_school)}}">{{$oNews->school_name}}</a></li>
			    <li class="active">Aktualności - {{strip_tags(Str::words($oNews->content, 5))}}</li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-5">
			<section>
				<h3>Kierunki</h3>
				<div>
					@if($aSpecializations->isEmpty())
						<div class="alert alert-info">Niestety obecnie nie prowadzimy żadnych kierunków. Zaprawszamy za jakiś czas.</div>
					@else
						@foreach($aSpecializations as $specialization)
						<a class="specialization-link" href="{{route('specialization', $specialization->id)}}">
							<span class="name">{{HTML::image('img/specializations/'.$specialization->filename, '', ['class' => 'img-thumbnail', 'width' => 120])}} {{$specialization->name}}</span>
						</a>
						@endforeach
					@endif
				</div>
			</section>
		</div>
		<div class="col-lg-7">
			<section id="news">
				<header><h4>{{strip_tags(Str::words($oNews->content, 5))}}</h4></header>
				<article>
					<div>
						<?php $aDate = News::printDate($oNews->created_at) ?>
						<div class="pull-left news-date">{{$aDate['day']}}<br>{{$aDate['month']}}</div>
						<section>{{$oNews->content}}</section>
						<footer><a href="{{route('school', $oNews->id_school)}}" class="pull-right more" title="Kliknij, aby przeczytać więcej">powrót...</a></footer>
					</div>
				</article>
			</section>
		</div>
	</div>
</div>

@stop