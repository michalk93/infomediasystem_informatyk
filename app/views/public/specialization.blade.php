@extends('public.template')

@section('title')
<title>{{$oSpecialization->name}} - Beskidzka Szkoła Zawodowa</title>
@stop

@section('description')
<meta name="description" content="{{$oSpecialization->school_name}} w Beskidzkiej Szkole Zawodowej w Suchej Beskidzkiej oferuje kształcenie w kierunku:  {{$oSpecialization->name}}. Kliknij, aby dowiedzieć się więcej...">
@stop

@section('keywords')
<meta name="keywords" content="{{strip_tags(Str::words($oSpecialization->about, 10))}}, {{$oSpecialization->name}} sucha beskidzka, {{$oSpecialization->name}} powiat suski">
@stop

@section('content')

<div class="row white-box" style="padding: 20px">
	<div class="row">
		<div class="col-lg-12">
			<ul class="breadcrumb">
			    <li><a href="{{route('home')}}">Home</a></li>
			    <li><a href="{{route('school', $oSpecialization->id_school)}}">{{$oSpecialization->school_name}}</a></li>
			    <li class="active">{{$oSpecialization->name}}</li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-5">
			<section>
				<h3>Kierunki</h3>
				<div>
					@if($aSpecializations->isEmpty())
						<div class="alert alert-info">Niestety obecnie nie prowadzimy żadnych kierunków. Zaprawszamy za jakiś czas.</div>
					@else
						@foreach($aSpecializations as $specialization)
						<a class="specialization-link" href="{{route('specialization', $specialization->id)}}">
							<span class="name">{{HTML::image('img/specializations/'.$specialization->filename, '', ['class' => 'img-thumbnail', 'width' => 120])}} {{$specialization->name}}</span>
						</a>
						@endforeach
					@endif
				</div>
			</section>
		</div>
		<div class="col-lg-7">
			<h3>{{$oSpecialization->name}}</h3>
			<ul class="nav nav-tabs">
			    <li class="active"><a href="#about" data-toggle="tab">O kierunku</a></li>
			    <li><a href="#recruitment" data-toggle="tab">Rekrutacja</a></li>
			    <li><a href="#plan" data-toggle="tab">Plan nauczania</a></li>
			    <li><a href="#files" data-toggle="tab">Dokumenty/Pliki do pobrania</a></li>
			</ul>

			<div class="tab-content">
			 	<div class="tab-pane active" id="about">
			 		{{HTML::image('img/specializations/'.$oSpecialization->filename, '', ['class' => 'pull-right img-thumbnail'])}}
			 		{{$oSpecialization->about}} 
				</div>
			  	<div class="tab-pane" id="recruitment">
			  		{{$oSpecialization->recruitment}}
			  	</div>
			  	<div class="tab-pane" id="plan">
			  		{{$oSpecialization->teaching_plan}}
			  	</div>
			  	<div class="tab-pane" id="files">
			  		@foreach($aFileGroups as $groupName => $aFileGroup)
			  			<div class="panel-group" id="accordion">
						  <div class="panel">
						    <div class="panel-heading">
						      <h3 class="panel-title">
						        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#{{Str::slug($groupName)}}">
						          {{$groupName}}</small>
						        </a>
						      </h3>
						    </div>
						    <div id="{{Str::slug($groupName)}}" class="panel-collapse collapse in">
						      <div class="panel-body">
						      	@foreach ($aFileGroup as $oFile)
						      		<div class="row">
						      			<div class="col-lg-12">
						      				<a href="{{route('downloadfile', $oFile->id)}}" title="Kliknij, aby pobrać plik na komputer">
						      					{{HTML::image('img/general/icon-download.png', '')}} {{$oFile->description}}
						      				</a>
						      			</div>
						      		</div>
						      	@endforeach
						      </div>
						    </div>
						  </div>
						</div>
			  		@endforeach
			  	</div>
			</div>
		</div>
	</div>
</div>

@stop