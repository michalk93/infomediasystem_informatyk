<!DOCTYPE html>
<html class="no-js">
    <head>
    	<meta name="google-site-verification" content="giBxM3hebbSI7twrRTnFtCRlnncz2Wrvtm7XZLfYftA" />
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link href="{{url('favicon.ico')}}" rel="icon" type="image/x-icon" />
        @yield('description',  '<meta name="description" content="Beskidzka Szkoła Zawodowa to placówka dla osób dorosłych chcących zdobyć wykształcenie. W naszej ofercie dostępne są licea ogólnokształcące, technika oraz kilka kierunków medycznych, dostępnych w studium medycznym.">')
        @yield('keywords', '<meta name="keywords" content="szkoła dla dorosłych, beskidzka szkoła zawodowa, szkoła sucha beskidzka, zaoczna szkoła sucha beskidzka, technikum zaoczne, studium, liceum">')
        <meta name="viewport" content="width=device-width">
		
		{{HTML::style('css/bootstrap/bootstrap.min.css')}}
        {{HTML::style('css/public/main.css')}}
        @yield('css')

        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
        @yield('title', '<title>Beskidzka Szkoła Zawodowa</title>')
    </head>
    <body>
    	<div id="top-wrapper" class="wrapper">
	    	<div class="container">
				<header class="row">
					<div class="col-lg-9">
						<a href="{{route('home')}}" title="Strona główna">{{HTML::image('img/general/logo-shadow.png', '', array('class' => 'pull-left', 'id' => 'logo', 'height' => 160))}}
						<h1 id="page-title">BESKIDZKA SZKOŁA ZAWODOWA</h1></a>
						<address>
							<span>tel. 33 874 45 36, e-mail: bncdn@poczta.fm</span><br/>
							34-200 Sucha Beskidzka, ul. Kościelna 5 (budynek ZS im. W. Goetla, pokój 109)
						</address>
					</div>
					<aside class="col-lg-3 visible-lg">
						<blockquote>
						  <p><em>Nauka w szkołach powinna być prowadzona w taki sposób, aby uczniowie uważali ją za cenny dar,<br> a nie za ciężki obowiązek.</em></p>
						  <small>Albert Einstein</small>
						</blockquote>
					</aside>
					
				</header>
			</div>
		</div>
		
		<div id="main-wrapper" class="wrapper">
			<main class="container">
				@yield('content')
			</main>
		</div>
		<div id="footer-wrapper" class="wrapper">
			<footer class="container">
				<div class="row">
					<div class="col-lg-12 white-box">
						<a href="{{route('elearning')}}">Platforme e-learningowa</a>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 text-center">
						<p>Copyright &copy; {{date('Y')}} informatyk.civ.pl, All rights reserved</p>
					</div>
				</div>
				
			</footer>
		</div>
		
	        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
	        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
	        {{HTML::script('js/bootstrap.min.js')}}
	        {{ HTML::script('js/whcookies.js') }}
	        @yield('js')
    </body>
</html>
