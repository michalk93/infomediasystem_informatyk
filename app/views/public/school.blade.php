@extends('public.template')

@section('title')
<title>{{$oSchool->name}} - Beskidzka Szkoła Zawodowa</title>
@stop

@section('description')
<meta name="description" content="{{$oSchool->name}} dla dorosłych w BSZ oferuje różne kierunki kształcenia. Kliknij, aby sprawdzić ofertę...">
@stop

@section('keywords')
<meta name="keywords" content="{{$oSchool->name}} dla dorosłych,  {{$oSchool->name}} sucha beskidzka, {{$oSchool->name}} powiat suski">
@stop

@section('content')
<div class="row white-box" style="padding: 20px">
	<div class="row">
		<div class="col-lg-12">
			<ul class="breadcrumb">
			    <li><a href="{{route('home')}}">Home</a></li>
			    <li class="active">{{$oSchool->name}}</li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-5">
			<section>
				<h3>Kierunki</h3>
				<div>
					@if($aSpecializations->isEmpty())
						<div class="alert alert-info">Niestety obecnie nie prowadzimy żadnych kierunków. Zapraszamy za jakiś czas.</div>
					@else
						@foreach($aSpecializations as $oSpecialization)
						<a class="specialization-link" href="{{route('specialization', $oSpecialization->id)}}">
							<span class="name">{{HTML::image('img/specializations/'.$oSpecialization->filename, '', ['class' => 'img-thumbnail', 'width' => 120])}} {{$oSpecialization->name}}</span>
						</a>
						@endforeach
					@endif
				</div>
			</section>
		</div>
		<div class="col-lg-7">
			<section id="news">
				<header>
					<h3>Aktualności</h3>
				</header>
				@if($aNews->isEmpty())
					<div class="alert alert-info">Brak aktualności. Zapraszamy za jakiś czas.</div>
				@else
					@foreach ($aNews as $oNews)
						<article class="clearfix">
							<div>
								<?php $aDate = News::printDate($oNews->created_at) ?>
								<div class="pull-left news-date">{{$aDate['day']}}<br>{{$aDate['month']}}</div>
								<section>{{$oNews->content}}</section>
								<footer><a href="{{route('news', $oNews->id)}}" class="pull-right more" title="Kliknij, aby przeczytać więcej">więcej...</a></footer>
								<div class="clear"></div>
							</div>
						</article>
					@endforeach
				@endif
				<div class="clearfix text-center">
					{{$aNews->links()}}
				</div>
			</section>
		</div>
	</div>
</div>

@stop

@section('js')
	{{HTML::script('js/jquery.ellipsis.js')}}
	<script>
		/*$('section#news article section').ellipsis({
			row: 3
		})*/
	</script>
@stop