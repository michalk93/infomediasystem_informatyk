@extends('public.template')

@section('content')
<div class="row">
	<div class="col-sm-4">
		<a href="{{route('school', $aSchools[0]->id)}}">
			<section class="school-block white-box">
				<div class="school-name">
					<h2>{{$aSchools[0]->name}} Ogólnokształcące</h2>
				</div>
				{{HTML::image('img/general/study1.jpg', 'Studium', ['class' => 'img-responsive'])}}
				<div class="school-description">
					<h3>Szkoła dwujęzyczna</h3>
					<p class="expanded">wybrane zajęcia w języku angielskim</p>
				</div>
			</section>
		</a>
	</div>
	<div class="col-sm-4">
		<a href="{{route('school', $aSchools[1]->id)}}">
			<section class="school-block white-box">
				<div class="school-name">
					<h2>{{$aSchools[1]->name}}</h2>
				</div>
				{{HTML::image('img/general/study2.jpg', 'Studium', ['class' => 'img-responsive'])}}
				<div class="school-description">
					<h3>Technik informatyk kierunek dwujęzyczny</h3>
					<p class="expanded">wybrane zajęcia w języku angielskim</p>
				</div>
			</section>
		</a>
	</div>
	<div class="col-sm-4">
		<a href="{{route('school', $aSchools[2]->id)}}">
			<section class="school-block white-box">
				<div class="school-name">
					<h2>{{$aSchools[2]->name}}</h2>
				</div>
				{{HTML::image('img/general/study3.jpg', 'Studium medyczne', ['class' => 'img-responsive'])}}
				<div class="school-description">
					<h3>Asystentka stomatologiczna</h3>
					<p class="expanded">ponownie zapraszamy na ten kierunek</p>
				</div>
			</section>
		</a>
	</div>
</div>
@if(!$aSlides->isEmpty())
<div class="row">
	<div class="col-lg-12">
		<div id="carousel-example-generic" class="carousel slide">
		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">
		  		@foreach($aSlides as $slide)
		  			<div class="item">
					<a href="{{$slide->url}}">{{HTML::image("img/slides/$slide->filename")}}</a>
					<div class="carousel-caption"></div>
		    		</div>
		  		@endforeach
		  </div>

		  <!-- Controls -->
		  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
		    <span class="icon-prev"></span>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
		    <span class="icon-next"></span>
		  </a>
		</div>
	</div>
</div>
@endif
<div class="row" style="margin-top: 20px; text-align: center">
	<div class="col-lg-8 col-lg-offset-2" id="social-icons">
		<h3>Przyłącz się do nas na Facebooku</h3>
		<div>
			<a href="https://www.facebook.com/pages/Beskidzka-Szko%C5%82a-Zawodowa/335060136579292" title="Polub nas na Facebook'u" target="_blank">{{HTML::image('img/general/facebook.png', 'Facebook', array('height' => 64))}}</a>
		</div>
		<div>
			<div class="fb-like" data-href="http://facebook.com/335060136579292" data-width="450" data-show-faces="false" data-send="true" style="margin: 10px;"></div>
			<script>
				(function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) return;
					js = d.createElement(s); js.id = id;
					js.src = "//connect.facebook.net/pl_PL/all.js#xfbml=1";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
		</div>
	</div>

</div>
@stop


@section('js')
	{{HTML::script('js/jquery.cycle.all.js')}}
	<script type="text/javascript">

	$('.cycle-slideshow').attr({'data-cycle-timeout' : '3000', "data-cycle-speed" : "1000"});

	$('section.school-block').on('mouseenter', function(){
		$(this).addClass('hover');
		$('section.school-block:not(.hover)').stop().fadeTo(500, 0.2);
	}).on('mouseleave', function(){
		$('section.school-block:not(.hover)').stop().fadeTo(500, 1);
		$(this).removeClass('hover');
	});


	var carouselItems = $('#carousel-example-generic > div.carousel-inner').children();
	var x = Math.floor(Math.random()*carouselItems.length);
	$(carouselItems[x]).addClass('active');
	$('#carousel-example-generic').carousel('cycle');

	
	</script>
@stop

@section('css')
	{{HTML::style('css/animate.css')}}
@stop