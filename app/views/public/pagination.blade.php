<ul class="pagination">
		@if ($paginator->getCurrentPage() <= 1)
			<li class="disabled"><span>&laquo;</span></li>
		@else
			<li><a href="{{$paginator->getUrl(1)}}">&laquo;</a></li>
		
		@endif

		@for ($page = 1; $page <= $paginator->getLastPage(); $page++)
			@if ($paginator->getCurrentPage() == $page)
				<li class="active"><span>{{$page}}</span></li>
			@else
				<li><a href="{{$paginator->getUrl($page)}}">{{$page}}</a></li>
			@endif
		@endfor
		
		@if ($paginator->getCurrentPage() >= $paginator->getLastPage())
			<li class="disabled"><span>&raquo;</span></li>
		@else
			<li><a href="{{$paginator->getUrl($paginator->getLastPage())}}">&raquo;</a></li>
		@endif
</ul>