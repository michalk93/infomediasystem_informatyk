@extends('admin.template')

@section('content')
@if(Session::has('message'))
<div class="row">
	<div class="col-lg-12">
		<div class="alert alert-info">
			<ul>
				<li>{{Session::get('message')}}</li>
			</ul>
		</div>
	</div>
</div>
@endif
<div class="row">
	<div class="col-lg-12">
		<table class="table table-striped" id="newsTable">
			<tr>
				<th>ID</th>
				<th>Podgląd</th>
				<th>Nazwa pliku</th>
				<th>Adres/odnośnik</th>
				<th>Menu</th>
			</tr>
			@if($aSlides->isEmpty())
			<tr>
				<td colspan="5"><p class="alert alert-info">Brak slajdów na stronie głównej</p></td>
			</tr>
			@else
				@foreach ($aSlides as $slide)
				<tr>
					<td class="col-lg-1">{{$slide->id}}</td>
					<td class="col-lg-3">{{HTML::image(Slides::$path.$slide->filename, $slide->filename, array('height=50'))}}</td>
					<td class="col-lg-4"><div class="content">{{$slide->filename}}</div></td>
					<td class="col-lg-4"><div class="content"><a href="{{$slide->url}}" target="_blank">Kliknij, aby zobaczyć</a></div></td>	
					<td>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
								Akcja <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="{{route('admin-editslide', $slide->id)}}">Edytuj</a></li>
								<li><a href="{{route('admin-deleteslide', $slide->id)}}">Usuń</a></li>
							</ul>
						</div>
					</td>
				</tr>
				@endforeach	
			@endif
		</table>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		{{$aSlides->links()}}
	</div>
</div>
@stop

@section('js')
	<script>
	$('ul.dropdown-menu a.more').on('click', function(){
		if($(this).hasClass('discover')){
			$(this).removeClass('discover').text('Pokaż całość').parents('tr').find('div.content').css({'height': '20px'});
		}else {
			$(this).addClass('discover').text('Schowaj').parents('tr').find('div.content').css({'height': '100%'});
		}
	})

	</script>
@stop