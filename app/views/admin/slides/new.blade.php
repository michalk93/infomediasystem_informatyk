@extends('admin.template')

@section('content')
@if($errors->any())
<div class="row">
	<div class="col-lg-12">
		<div class="alert alert-danger">Formularz zawiera błędy!</div>
	</div>
</div>
@endif
<div class="row">
	<div class="col-lg-12">
		<h3>Dodawanie slajdu</h3>
		<hr>
	</div>
</div>
<div class="row">
	<form action="{{route('admin-newslide-post')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
		<div class="form-group">
			<label for="inputFile" class="col-lg-2 control-label">Plik</label>
			<div class="col-lg-4">
				<input type="file" name="file" class="form-control" id="inputFile" required>
				<p class="help-block">Maksymalny rozmiar: 500 KB</p>
			</div>
			{{MyValidator::printMessage($errors, 'file')}}
		</div>
		<div class="form-group">
			<label for="slideURL" class="col-lg-2 control-label">Adres URL</label>
			<div class="col-lg-4">
				<input type="text" name="slideURL" class="form-control" id="slideURL" required>
				<p class="help-block">Adres, do którego nastąpi przekierowanie po kliknięciu w&nbsp;slajd</p>
			</div>
			{{MyValidator::printMessage($errors, 'slideURL')}}
		</div>
		<div class="form-group">
		<div class="col-lg-offset-2 col-lg-10">
			<button type="submit" class="btn btn-default">Dodaj</button>
		</div>
		</div>
	</form>

</div>
@stop

@section('js')
{{HTML::script('js/ckeditor/ckeditor.js')}}
<script type="text/javascript">
	CKEDITOR.replace( 'ckeditor');
</script>
@stop