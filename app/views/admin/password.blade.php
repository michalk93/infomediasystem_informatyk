@extends('admin.template')

@section('content')
<div class="row">
<div class="well text-center">
	<strong>ZMIANA HASŁA DLA {{ Auth::user()->email }}</strong>
</div>

{{ Form::open(array(
	'route' => array('admin-change-password-post', Auth::user()->id),
	'method' => 'POST',
	'class' => 'form-horizontal'
)); }}

<div class="row well">
	<div class="form-group">
		<label for="inputPassword" class="col-lg-2 control-label">Nowe hasło</label>
		<div class="col-lg-10">
			<input type="password" name="password" class="form-control" id="inputPassword">
		</div>
		{{ MyValidator::printMessage($errors, 'password') }}
	</div>

	<div class="form-group">
		<label for="inputPasswordConf" class="col-lg-2 control-label">Powtórz hasło</label>
		<div class="col-lg-10">
			<input type="password" name="password_confirmation" class="form-control" id="inputPasswordConf">
		</div>
		{{ MyValidator::printMessage($errors, 'password_confirmation') }}
	</div>
	<div class="control-group">
		<div class="controls">
			<button type="submit" class="btn btn-primary form-control">Zmień hasło</button>
		</div>
	</div>
</div>
{{ Form::close() }}
</div>
@stop