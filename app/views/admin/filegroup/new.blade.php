@extends('admin.template')

@section('content')
@if($errors->any())
<div class="row">
	<div class="col-lg-12">
		<div class="alert alert-danger">Formularz zawiera błędy!</div>
	</div>
</div>
@endif
<div class="row">
	<div class="col-lg-12">
		<h3>Dodawanie grupy</h3>
		<hr>
	</div>
</div>
<div class="row">

	<form action="{{route('admin-newfilegroup-post')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
		<div class="form-group">
			<label for="inputName" class="col-lg-2 control-label">Nazwa grupy</label>
			<div class="col-lg-4">
				<input type="text" name="name" id-"inputName" maxlength="60" class="form-control" value="{{Input::old('name')}}" autofocus="autofocus" required>
			</div>
			{{MyValidator::printMessage($errors, 'name')}}
			<button type="submit" class="btn btn-default">Dodaj</button>
		</div>
	</form>

</div>
@stop
