@extends('admin.template')

@section('content')
@if(Session::has('message'))
<div class="row">
	<div class="col-lg-12">
		<div class="alert alert-info">
			<ul>
				<li>{{Session::get('message')}}</li>
			</ul>
		</div>
	</div>
</div>
@endif
<div class="row">
	<div class="col-lg-12">
		<table class="table table-striped" id="newsTable">
			<tr>
				<th>ID</th>
				<th>Nazwa grupy</th>
				<th>Menu</th>
			</tr>
			@if($aFilegroups->isEmpty())
			<tr>
				<td colspan="3"><p class="alert alert-info">Baza grup jest pusta.</p></td>
			</tr>
			@else
				@foreach ($aFilegroups as $oFilegroup)
				<tr>
					<td class="col-lg-4">{{$oFilegroup->id}}</td>
					<td class="col-lg-6">{{$oFilegroup->name}}</td>
					<td>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
								Akcja <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="{{route('admin-editfilegroup', $oFilegroup->id)}}">Edytuj</a></li>
								<li><a href="{{route('admin-deletefilegroup', $oFilegroup->id)}}">Usuń</a></li>
							</ul>
						</div>
					</td>
				</tr>
				@endforeach	
			@endif
		</table>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		{{$aFilegroups->links()}}
	</div>
</div>
@stop