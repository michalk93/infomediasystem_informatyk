<!DOCTYPE html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link href="{{url('favicon.ico')}}" rel="icon" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
	
	{{HTML::style('css/bootstrap/bootstrap.min.css')}}

    <!--[if lt IE 9]>
        <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
    <![endif]-->
     <title>Panel administracyjny | Beskidzka Szkoła Zawodowa</title>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-lg-12 well text-center">
			{{HTML::image('img/general/logo-shadow.png', '', ['height' => 128])}}
			<h1>BESKIDZKA SZKOŁA ZAWODOWA</h1>
		</div>
  	</div>
  	<form class="form-horizontal text-center" action="{{ URL::route('admin-login') }}" method="post" >
  		<fieldset>
		<div class="row">
			<div class="col-lg-12">
				<div id="legend">
		        <legend class="text-center">Logowanie do panelu administracyjnego</legend>
				</div>
				@if (isset($message))
				<div class="alert alert-error">
					{{ $message }}
				</div>
				@endif
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-lg-offset-4">
				<div class="form-group">
					<!-- Username -->
					<label class="control-label"  for="username">Adres e-mail</label>
					<input type="email" id="email" name="email" placeholder="Wprowadź adres e-mail..." class="form-control" autofocus="autofocus" tabindex="1">
				</div>

				<div class="form-group">
					<!-- Password-->
					<label class="control-label" for="password">Hasło</label>
					<input type="password" id="password" name="password" placeholder="Wprowadź hasło..." class="form-control" tabindex="2">
				</div>


				<div class="form-group">
				<!-- Button -->
				<div class="controls">
				<button class="btn btn-success" tabindex="3">Zaloguj</button>
				</div>
				</div>
			</div>
    	</div>
     
		
		</fieldset>
	</form>
</div>
</body>
</html>