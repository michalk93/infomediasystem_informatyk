@extends('admin.template')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<!--<div class="alert alert-warning"></div>-->
	</div>
</div>
<div class="row">
	<div class="col-lg-6" id="shortcut-links">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Szybki dostęp</h3>
			</div>
			<div class="panel-body">
				<a href="{{route('admin-newspecialization')}}">
					<span>{{HTML::image('img/general/icon-add.png', '')}}</span>
					<span>Dodaj kierunek</span>
				</a>
				<a href="{{route('admin-newnews')}}">
					<span>{{HTML::image('img/general/icon-add.png', '')}}</span>
					<span>Dodaj news'a</span>
				</a>
				<a href="{{route('admin-newslide')}}">
					<span>{{HTML::image('img/general/icon-add.png', '')}}</span>
					<span>Dodaj slajd</span>
				</a>
				<hr>
				<a href="{{route('admin-specialization')}}">
					<span>{{HTML::image('img/general/icon-explore.png', '')}}</span>
					<span>Przeglądaj kierunki</span>
				</a>
				<a href="{{route('admin-news')}}">
					<span>{{HTML::image('img/general/icon-explore.png', '')}}</span>
					<span>Przeglądaj aktualności</span>
				</a>
				<a href="{{route('admin-slides')}}">
					<span>{{HTML::image('img/general/icon-explore.png', '')}}</span>
					<span>Przeglądaj slajdy</span>
				</a>
			</div>
		</div>
	</div>
	<div class="col-lg-6" id="statistics">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Statystyki</h3>
			</div>
			<div class="panel-body">
				<table class="table">
					<tr>
						<td>Ilość wszystkich szkół</td>
						<td class="col-lg-2">{{$aStatistics['schools']['count']}}</td>
					</tr>
					<tr>
						<td>Ilość wszystkich kierunków</td>
						<td class="col-lg-2">{{$aStatistics['specializations']['count']}}</td>
					</tr>
					@foreach ($aStatistics['schools']['specializations'] as $school => $count)
						<tr>
							<td>{{$school}} - ilość kierunków</td>
							<td class="col-lg-2">{{$count['count']}}</td>
						</tr >
					@endforeach
				</table>
				<table class="table">
					<tr>
						<td>Ilość aktualności</td>
						<td class="col-lg-2">{{$aStatistics['news']['count']}}</td>
					</tr>
					@foreach ($aStatistics['news']['schools'] as $school => $count)
						<tr>
							<td>{{$school}} - ilość aktualności</td>
							<td class="col-lg-2">{{$count['count']}}</td>
						</tr>
					@endforeach
				</table>
				<table class="table">
					<tr>
						<td>Ilość wszystkich plików</td>
						<td class="col-lg-2">{{$aStatistics['files']['count']}}</td>
					</tr>
					<tr>
						<td>Ilość stworzonych grup</td>
						<td class="col-lg-2">{{$aStatistics['files']['groups']['count']}}</td>
					</tr>
					<tr>
						<td>Rozmiar zajmowany przez wszystkie pliki</td>
						<td class="col-lg-2">{{$aStatistics['files']['size']}} MB</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
@stop