@extends('admin.template')

@section('content')
@if($errors->any())
<div class="row">
	<div class="col-lg-12">
		<div class="alert alert-danger">Formularz zawiera błędy!</div>
	</div>
</div>
@endif
<div class="row">
	<div class="col-lg-12">
		<h3>Aktualizowanie news'a</h3>
		<hr>
	</div>
</div>
<div class="row">
	<form action="{{route('admin-editnews-post', $oNews->id)}}" class="form-horizontal" method="post">
		<div class="form-group">
			<label for="inputSchool" class="col-lg-2 control-label">Typ szkoły</label>
			<div class="col-lg-4">
			@if($aSchools->isEmpty())
				<p class="alert alert-warning">Brak szkół. Dodaj szkołę, aby kontynuować.</p>
			@else
			<select name="id_school" class="form-control" id="inputSchool" autofocus="autofocus" required>
				<option value="">Wybierz typ szkoły...</option>
				@foreach($aSchools as $oSchool)
					@if($oNews->id_school == $oSchool->id)
						<option value="{{$oSchool->id}}" selected="selected">{{$oSchool->name}}</option>
					@else
						<option value="{{$oSchool->id}}">{{$oSchool->name}}</option>
					@endif
				@endforeach
			</select>
			@endif
			</div>
			{{MyValidator::printMessage($errors, 'id_school')}}
		</div>
		<div class="form-group">
			<label for="inputName" class="col-lg-2 control-label">Treść news'a</label>
			<div class="col-lg-10">
				<textarea name="content" id="ckeditor" required>{{Input::old('content') ?: $oNews->content}}</textarea>
			</div>
			{{MyValidator::printMessage($errors, 'content')}}
		</div>
		<div class="form-group">
		<div class="col-lg-offset-2 col-lg-10">
		<button type="submit" class="btn btn-default">Aktualizuj</button>
		</div>
		</div>
	</form>

</div>
@stop

@section('js')
{{HTML::script('js/ckeditor/ckeditor.js')}}
<script type="text/javascript">
	CKEDITOR.replace( 'ckeditor');
</script>
@stop