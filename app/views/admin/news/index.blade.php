@extends('admin.template')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<table class="table table-striped" id="newsTable">
			<tr>
				<th>ID</th>
				<th>Treść</th>
				<th>Typ szkoły</th>
				<th>Data</th>
				<th>Menu</th>
			</tr>
			@if($aNews->isEmpty())
			<tr>
				<td colspan="5"><p class="alert alert-info">Baza aktualności jest pusta.</p></td>
			</tr>
			@else
				@foreach ($aNews as $oNews)
				<tr>
					<td class="col-lg-1">{{$oNews->id}}</td>
					<td class="col-lg-6"><div class="content">{{$oNews->content}}</div></td>
					<td class="col-lg-2">{{$oNews->school_name}}</td>
					<td class="col'lg-2">{{$oNews->created_at}}</td>
					<td>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
								Akcja <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="#" class="more">Pokaż całość</a></li>
								<li><a href="{{route('admin-editnews', $oNews->id)}}">Edytuj</a></li>
								<li><a href="{{route('admin-deletenews', $oNews->id)}}">Usuń</a></li>
							</ul>
						</div>
					</td>
				</tr>
				@endforeach	
			@endif
		</table>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		{{$aNews->links()}}
	</div>
</div>
@stop

@section('js')
	<script>
	$('ul.dropdown-menu a.more').on('click', function(){
		if($(this).hasClass('discover')){
			$(this).removeClass('discover').text('Pokaż całość').parents('tr').find('div.content').css({'height': '20px'});
		}else {
			$(this).addClass('discover').text('Schowaj').parents('tr').find('div.content').css({'height': '100%'});
		}
	})

	</script>
@stop