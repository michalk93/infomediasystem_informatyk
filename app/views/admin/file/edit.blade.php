@extends('admin.template')

@section('content')
@if($errors->any())
<div class="row">
	<div class="col-lg-12">
		<div class="alert alert-danger">Formularz zawiera błędy!</div>
	</div>
</div>
@endif
<div class="row">
	<div class="col-lg-12">
		<h3>Aktualizowanie pliku</h3>
		<hr>
	</div>
</div>
<div class="row">

	<form action="{{route('admin-editfile-post')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
		<div class="form-group">
			<label for="inputSpecialization" class="col-lg-2 control-label">Kierunek</label>
			<div class="col-lg-4">
			@if($aSpecializations->isEmpty())
				<p class="alert alert-warning">Brak kierunków. Dodaj kierunek, aby kontynuować.</p>
			@else
			<select name="id_specialization" class="form-control" id="inputSpecialization" autofocus="autofocus" required>
				<option value="">Wybierz kierunek...</option>
				@foreach($aSpecializations as $oSpecialization)
					@if($oFile->id_specialization == $oSpecialization->id)
					<option value="{{$oSpecialization->id}}" selected>{{$oSpecialization->name}}</option>
					@else
					<option value="{{$oSpecialization->id}}">{{$oSpecialization->name}}</option>
					@endif
				@endforeach
			</select>
			@endif
			</div>
			{{MyValidator::printMessage($errors, 'id_specialization')}}
		</div>
		<div class="form-group">
			<label for="inputSpecialization" class="col-lg-2 control-label">Grupa</label>
			<div class="col-lg-4">
			@if($aFileGroups->isEmpty())
				<p class="alert alert-warning">Brak grup. Plik będzie wyświetlany w kategorii pozostałe.</p>
			@else
			<select name="id_filegroup" class="form-control" id="inputSpecialization" required>
				<option value="">Wybierz grupę...</option>
				@foreach($aFileGroups as $aFileGroup)
					@if($oFile->id_filegroup == $aFileGroup->id)
					<option value="{{$aFileGroup->id}}" selected>{{$aFileGroup->name}}</option>
					@else
					<option value="{{$aFileGroup->id}}">{{$aFileGroup->name}}</option>
					@endif
				@endforeach
			</select>
			@endif
			</div>
			{{MyValidator::printMessage($errors, 'id_filegroup')}}
		</div>
		<div class="form-group">
			<label for="inputFile" class="col-lg-2 control-label">Opis pliku</label>
			<div class="col-lg-4">
				<input type="text" name="description" id-"inputDescription" maxlength="255" class="form-control" value="{{$oFile->description}}" required>
				<p class="help-block">Opis będzie wyświetlany na stronie.<br> Dozwolona ilość znaków: 255</p>
			</div>
			{{MyValidator::printMessage($errors, 'description')}}
		</div>
		<div class="form-group">
			<label for="inputFile" class="col-lg-2 control-label">Istniejący plik</label>
			<div class="col-lg-4">
				<input type="text" class="form-control" value="{{$oFile->filename}}" disabled>
			</div>
		</div>
		<div class="form-group">
			<label for="inputFile" class="col-lg-2 control-label">Nowy plik</label>
			<div class="col-lg-4">
				<input type="file" name="file" class="form-control">
				<p class="help-block">Maksymalny rozmiar: 500 KB</p>
			</div>
			{{MyValidator::printMessage($errors, 'file')}}
		</div>
		<div class="form-group">
		<div class="col-lg-offset-2 col-lg-10">
			<input type="hidden" name="id" value="{{ $oFile->id }}">
			<button type="submit" class="btn btn-default">Aktualizuj</button>
		</div>
		</div>
	</form>

</div>
@stop
