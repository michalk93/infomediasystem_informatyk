@extends('admin.template')

@section('content')
@if($errors->any())
<div class="row">
	<div class="col-lg-12">
		<div class="alert alert-danger">Formularz zawiera błędy!</div>
	</div>
</div>
@endif
<div class="row">
	<div class="col-lg-12">
		<h3>Dodawanie pliku</h3>
		<hr>
	</div>
</div>
<div class="row">

	<form action="{{route('admin-newfile-post')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
		<div class="form-group">
			<label for="inputSpecialization" class="col-lg-2 control-label">Kierunek</label>
			<div class="col-lg-4">
			@if($aSpecializations->isEmpty())
				<p class="alert alert-warning">Brak kierunków. Dodaj kierunek, aby kontynuować.</p>
			@else
			<select name="id_specialization" class="form-control" id="inputSpecialization" autofocus="autofocus" required>
				<option value="">Wybierz kierunek...</option>
				@foreach($aSpecializations as $oSpecialization)
				<option value="{{$oSpecialization->id}}">{{$oSpecialization->name}}</option>
				@endforeach
			</select>
			@endif
			</div>
			{{MyValidator::printMessage($errors, 'id_specialization')}}
		</div>
		<div class="form-group">
			<label for="inputSpecialization" class="col-lg-2 control-label">Grupa</label>
			<div class="col-lg-4">
			@if($aFileGroups->isEmpty())
				<p class="alert alert-warning">Brak grup. Plik będzie wyświetlany w kategorii pozostałe.</p>
			@else
			<select name="id_filegroup" class="form-control" id="inputSpecialization">
				<option value="">Wybierz grupę...</option>
				@foreach($aFileGroups as $aFileGroup)
				<option value="{{$aFileGroup->id}}">{{$aFileGroup->name}}</option>
				@endforeach
			</select>
			@endif
			</div>
			{{MyValidator::printMessage($errors, 'id_filegroup')}}
		</div>
		<div class="form-group">
			<label for="inputFile" class="col-lg-2 control-label">Opis pliku</label>
			<div class="col-lg-4">
				<input type="text" name="description" id-"inputDescription" maxlength="255" class="form-control" value="{{Input::old('description')}}" required>
				<p class="help-block">Opis będzie wyświetlany na stronie.<br> Dozwolona ilość znaków: 255</p>
			</div>
			{{MyValidator::printMessage($errors, 'description')}}
		</div>
		<div class="form-group">
			<label for="inputFile" class="col-lg-2 control-label">Plik</label>
			<div class="col-lg-4">
				<input type="file" name="file" class="form-control" required>
				<p class="help-block">Maksymalny rozmiar: 500 KB</p>
			</div>
			{{MyValidator::printMessage($errors, 'file')}}
		</div>
		<div class="form-group">
		<div class="col-lg-offset-2 col-lg-10">
			<button type="submit" class="btn btn-default">Dodaj</button>
		</div>
		</div>
	</form>

</div>
@stop
