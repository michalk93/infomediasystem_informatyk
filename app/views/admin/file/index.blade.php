@extends('admin.template')

@section('content')
@if(Session::has('message'))
<div class="row">
	<div class="col-lg-12">
		<div class="alert alert-info">
			<ul>
				<li>{{Session::get('message')}}</li>
			</ul>
		</div>
	</div>
</div>
@endif
<div class="row">
	<div class="col-lg-12">
		{{Form::open(array('id' => 'filter-form', 'method' => 'get'))}}
		<input type="submit" value="" style="visibility: hidden">
		<table class="table table-striped" id="newsTable">
			
			<tr>
				
				<th></th>
				<th>
					<input type="text" class="form-control" name="filter-description" value="{{Session::get('filter-description', '')}}" placeholder="Opis pliku lub jego fragment...">
				</th>
				<th></th>
				<th>
					<input type="text" class="form-control" name="filter-specialization" value="{{Session::get('filter-specialization', '')}}" placeholder="Nazwa specializacji lub jej fragment...">
				</th>
				<th></th>
				<th>
					<div class="btn-group">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
							Filtr <span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<li><a href="#" id="setFilterButton">Zastosuj</a></li>
							<li><a href="#" id="clearFilterButton">Wyczyść</a></li>
						</ul>
					</div>
				</th>
				<input type="hidden" name="filter" value="true">
				
			</tr>
			
			<tr>
				<th>ID</th>
				<th>Opis pliku</th>
				<th>Nazwa pliku</th>
				<th>Kierunek</th>
				<th>Data</th>
				<th>Menu</th>
			</tr>
			@if($aFiles->isEmpty())
			<tr>
				<td colspan="6"><p class="alert alert-info">Baza plików jest pusta.</p></td>
			</tr>
			@else
				@foreach ($aFiles as $oFile)
				<tr>
					<td>{{$oFile->id}}</td>
					<td class="col-lg-3">{{$oFile->description}}</td>
					<td class="col-lg-3">{{$oFile->filename}}</td>
					<td class="col-lg-3">{{$oFile->specialization_name}}</td>
					<td class="col'lg-3">{{$oFile->updated_at}}</td>
					<td>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
								Akcja <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="{{route('admin-editfile', $oFile->id)}}">Edytuj</a></li>
								<li><a href="{{route('admin-filedownload', $oFile->id)}}">Pobierz</a></li>
								<li><a href="{{route('admin-deletefile', $oFile->id)}}">Usuń</a></li>
							</ul>
						</div>
					</td>
				</tr>
				@endforeach	
			@endif
		</table>
		{{Form::close()}}
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		{{$aFiles->links()}}
	</div>
</div>
@stop

@section('js')
<script>
	$('a#setFilterButton').click(function(){
		$('form#filter-form').submit();
	})

	$('a#clearFilterButton').click(function(){
		$('form#filter-form input:text').val("");
		$('form#filter-form').submit();
	});
</script>
@stop