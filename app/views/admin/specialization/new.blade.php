@extends('admin.template')

@section('content')
@if($errors->any())
<div class="row">
	<div class="col-lg-12">
		<div class="alert alert-danger">Formularz zawiera błędy!</div>
	</div>
</div>
@endif
<div class="row">
	<div class="col-lg-12">
		<h3>Dodawanie kierunku</h3>
		<hr>
	</div>
</div>
<div class="row">
<form class="form-horizontal" method="post" action="{{route('admin-newspecialization-post')}}" enctype="multipart/form-data">

	<ul class="nav nav-tabs" style="margin-bottom: 20px">
	  <li class="active"><a href="#general" data-toggle="tab">Ogólne (wymagane)</a></li>
	  <li><a href="#about" data-toggle="tab">O kierunku</a></li>
	  <li><a href="#recruitment" data-toggle="tab">Rekrutacja</a></li>
	  <li><a href="#plan" data-toggle="tab">Plan nauczania</a></li>
	  <button type="submit" class="btn btn-default pull-right">Dodaj kierunek</button>
	</ul>

	
	<div class="tab-content">

		<!-- General specialization -->
		<div class="tab-pane active" id="general">
			<div class="form-group">
				<label for="inputName" class="col-lg-2 control-label">Nazwa kierunku</label>
				<div class="col-lg-4">
					<input type="text" class="form-control" id="inputName" name="name" placeholder="" value="{{Input::old('name')}}" autofocus="autofocus" required>
				</div>
				{{MyValidator::printMessage($errors, 'name')}}
			</div>
			<div class="form-group">
				<label for="inputSchool" class="col-lg-2 control-label">Typ szkoły</label>
				<div class="col-lg-4">
				@if($aSchools->isEmpty())
					<p class="alert alert-warning">Brak szkół. Dodaj szkołę, aby kontynuować.</p>
				@else
				<select name="id_school" class="form-control" id="inputSchool" required>
					<option value="">Wybierz typ szkoły...</option>
					@foreach($aSchools as $oSchool)
					<option value="{{$oSchool->id}}">{{$oSchool->name}}</option>
					@endforeach
				</select>
				@endif
				</div>
				{{MyValidator::printMessage($errors, 'id_school')}}
			</div>
			<div class="form-group">
				<label for="exampleInputFile" class="col-lg-2 control-label">Grafika</label>
				<div class="col-lg-4">
					<input class="form-control" name="file" type="file" accept="image/jpeg" required>
					<p class="help-block">Rozmiar grafiki: 240px x 160px | Typ pliku: jpg</p>
				</div>
				{{MyValidator::printMessage($errors, 'file')}}
			</div>
		</div>

		<!-- About specialization -->
		<div class="tab-pane" id="about">
			<div class="form-group">
				<label for="exampleInputFile" class="col-lg-2 control-label">O kierunku</label>
				<div class="col-lg-10">
					<textarea name="about" id="ckedito1r" class="ckeditor">{{Input::old('about')}}</textarea>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="recruitment">
			<div class="form-group">
				<label for="exampleInputFile" class="col-lg-2 control-label">Informacje dotyczące rekrutacji</label>
				<div class="col-lg-10">
					<textarea name="recruitment" id="ckeditor2" class="ckeditor">{{Input::old('recruitment')}}</textarea>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="plan">
			<div class="form-group">
				<label for="exampleInputFile" class="col-lg-2 control-label">Plan nauczania</label>
				<div class="col-lg-10">
					<textarea name="teaching_plan" id="ckeditor3" class="ckeditor">{{Input::old('teaching_plan')}}</textarea>
				</div>
			</div>
		</div>
	</div>
	
</form>	
</div>
@stop

@section('js')
{{HTML::script('js/ckeditor/ckeditor.js')}}
<script>
	CKEDITOR.replace('.ckeditor1');
</script>
@stop