@extends('admin.template')

@section('content')
@if($errors->any())
<div class="row">
	<div class="col-lg-12">
		<div class="alert alert-danger">Formularz zawiera błędy!</div>
	</div>
</div>
@endif
<div class="row">
	<div class="col-lg-12">
		<h3>Aktualizacja kierunku</h3>
		<hr>
	</div>
</div>
<div class="row">
<form class="form-horizontal" method="post" action="{{route('admin-editspecialization-post')}}" enctype="multipart/form-data">

	<ul class="nav nav-tabs" style="margin-bottom: 20px">
	  <li class="active"><a href="#general" data-toggle="tab">Ogólne</a></li>
	  <li><a href="#about" data-toggle="tab">O kierunku</a></li>
	  <li><a href="#recruitment" data-toggle="tab">Rekrutacja</a></li>
	  <li><a href="#plan" data-toggle="tab">Plan nauczania</a></li>
	  <button type="submit" class="btn btn-default pull-right">Aktualizuj kierunek</button>
	</ul>

	
	<div class="tab-content">

		<!-- General specialization -->
		<div class="tab-pane active" id="general">
			<div class="form-group">
				<label for="inputName" class="col-lg-2 control-label">Nazwa kierunku</label>
				<div class="col-lg-4">
					<input type="text" class="form-control" id="inputName" name="name" value="{{$oSpecialization->name}}" autofocus="autofocus" required>
				</div>
				{{MyValidator::printMessage($errors, 'name')}}
			</div>
			<div class="form-group">
				<label for="inputSchool" class="col-lg-2 control-label">Typ szkoły</label>
				<div class="col-lg-4">
				@if($aSchools->isEmpty())
					<p class="alert alert-warning">Brak szkół. Dodaj szkołę, aby kontynuować.</p>
				@else
				<select name="id_school" class="form-control" id="inputSchool">
					<option value="0">Wybierz typ szkoły...</option>
					@foreach($aSchools as $oSchool)
						@if($oSchool->id == $oSpecialization->id_school)
							<option value="{{$oSchool->id}}" selected="selected">{{$oSchool->name}}</option>
						@else
							<option value="{{$oSchool->id}}">{{$oSchool->name}}</option>
						@endif
					@endforeach
				</select>
				@endif
				</div>
				{{MyValidator::printMessage($errors, 'id_school')}}
			</div>
			<div class="form-group">
				<label for="exampleInputFile" class="col-lg-2 control-label">Aktualna grafika</label>
				<div class="col-lg-4">
					{{Form::hidden('old_filename', $oSpecialization->filename)}}
					{{HTML::image('img/specializations/'.$oSpecialization->filename, $oSpecialization->filename, ['class' => 'img-thumbnail'])}}
				</div>
			</div>
			<div class="form-group">
				<label for="exampleInputFile" class="col-lg-2 control-label">Nowa grafika</label>
				<div class="col-lg-4">
					<input class="form-control" name="file" type="file" accept="image/jpeg">
					<p class="help-block">Rozmiar grafiki: 240px x 160px | Typ pliku: jpg</p>
				</div>
				{{MyValidator::printMessage($errors, 'file')}}
			</div>
		</div>

		<!-- About specialization -->
		<div class="tab-pane" id="about">
			<div class="form-group">
				<label for="exampleInputFile" class="col-lg-2 control-label">O kierunku</label>
				<div class="col-lg-10">
					<textarea name="about" id="ckedito1r" class="ckeditor">{{$oSpecialization->about}}</textarea>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="recruitment">
			<div class="form-group">
				<label for="exampleInputFile" class="col-lg-2 control-label">Informacje dotyczące rekrutacji</label>
				<div class="col-lg-10">
					<textarea name="recruitment" id="ckeditor2" class="ckeditor">{{$oSpecialization->recruitment}}</textarea>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="plan">
			<div class="form-group">
				<label for="exampleInputFile" class="col-lg-2 control-label">Plan nauczania</label>
				<div class="col-lg-10">
					<textarea name="teaching_plan" id="ckeditor3" class="ckeditor">{{$oSpecialization->teaching_plan}}</textarea>
				</div>
			</div>
		</div>
	</div>
	{{Form::hidden('id', $oSpecialization->id)}}
</form>	
</div>
@stop

@section('js')
{{HTML::script('js/ckeditor/ckeditor.js')}}
<script>
	CKEDITOR.replace('.ckeditor1');

</script>
@stop