@extends('admin.template')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<table class="table table-striped" id="newsTable">
			<tr>
				<th>ID</th>
				<th>Nazwa kierunku</th>
				<th>Typ szkoły</th>
				<th>Menu</th>
			</tr>
			@if($aSpecializations->isEmpty())
			<tr>
				<td colspan="5"><p class="alert alert-info">Baza kierunków jest pusta.</p></td>
			</tr>
			@else
				@foreach ($aSpecializations as $oSpecialization)
				<tr>
					<td class="col-lg-2">{{$oSpecialization->id}}</td>
					<td class="col-lg-6">
						<div class="content">{{$oSpecialization->name}}</div>
					</td>
					<td class="col-lg-4">{{$oSpecialization->school_name}}</td>
					<td>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
								Akcja <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="{{route('admin-editspecialization', $oSpecialization->id)}}">Edytuj</a></li>
								<li><a href="{{route('admin-deletespecialization', $oSpecialization->id)}}">Usuń</a></li>
							</ul>
						</div>
					</td>
				</tr>
				@endforeach	
			@endif
		</table>
	</div>
</div>
@stop