<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link href="{{url('favicon.ico')}}" rel="icon" type="image/x-icon" />
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
		
		{{HTML::style('css/bootstrap/bootstrap.min.css')}}
        {{HTML::style('css/admin/main.css')}}

        @yield('css')

        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
        {{HTML::script('js/bootstrap.min.js')}}
         <title>Panel administracyjny | Beskidzka Szkoła Zawodowa</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="navbar">
                        <a class="navbar-brand" href="{{route('admin-home')}}">Administracja BSZ</a>
                        <ul class="nav navbar-nav">
                            <li><a href="{{route('admin-home')}}">Home</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Nowy <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{route('admin-newnews')}}">News</a></li>
                                    <li><a href="{{route('admin-newspecialization')}}">Kierunek</a></li>
                                    <li><a href="{{route('admin-newslide')}}">Slajd</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Zobacz <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{route('admin-news')}}">Aktualności</a></li>
                                    <li><a href="{{route('admin-specialization')}}">Kierunki</a></li>
                                    <li><a href="{{route('admin-slides')}}">Slajdy</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pliki <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{route('admin-files')}}">Przeglądaj pliki</a></li>
                                    <li><a href="{{route('admin-newfile')}}">Dodaj plik</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{route('admin-filegroup')}}">Przeglądaj grupy</a></li>
                                    <li><a href="{{route('admin-newfilegroup')}}">Dodaj grupę</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav pull-right">
                            <li><a href="{{route('home')}}" target="_blank">Podgląd</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{Auth::user()->email}} <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{route('admin-change-password', Auth::user()->id)}}">Zmień hasło</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{route('admin-logout')}}">Wyloguj</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>  
                </div>
            </div>
            @yield('content')
        </div>
        
        @yield('js')
    </body>
</html>
