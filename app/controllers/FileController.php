<?php

use Illuminate\Support\MessageBag as MessageBag;

class FileController extends BaseController {




	//--------------------------------------------------
	// BACK-END METHODS
	//--------------------------------------------------


	/**
	* Display list of existing files
	**/
	public function indexAdmin()
	{
		if(Input::has('filter')){
			foreach(Request::query() as $filter => $value){
				Session::put($filter, $value);
			}
		}
		$aFiles = SchoolFile::join('specialization', 'files.id_specialization', '=', 'specialization.id')
			->where('files.description', 'LIKE', '%'.Session::get('filter-description').'%')
			->where('specialization.name', 'LIKE', '%'.Session::get('filter-specialization').'%')
			->select('files.*', 'specialization.name AS specialization_name')
			->orderBy('files.updated_at', 'DESC')
			->paginate(10);	
		return View::make('admin.file.index')
			->with('aFiles', $aFiles);
	}


	/**
	* Display form to add new file
	**/
	public function getNew()
	{
		return View::make('admin.file.new')
			->with('aSpecializations', SchoolSpecialization::all())
			->with('aFileGroups', Filegroup::all());
	}


	/**
	* Processing adding new file
	**/
	public function postNew()
	{
		$validator = Validator::make(Input::all(), SchoolFile::$rules);

		if($validator->fails()){
			return Redirect::back()->withErrors($validator)->withInput(array('description'));
		}

		$filename = SchoolFile::upload(Input::file('file'))->getFilename();

		if(!$filename){
			$messageBag = new MessageBag(array('file' => 'Wystąpił błąd podczas przesyłania pliku. Spróbuj ponownie.'));
			return Redirect::back()->withErrors($messageBag)->withInput(array('description'));
		}
		Input::merge(array('filename' => $filename));
		SchoolFile::create(Input::except(array('file')));

		return Redirect::route('admin-files')->with('message', 'Plik został dodany pomyślnie');
	}

	/**
	* Download a file
	**/
	public function getDownload($id)
	{
		$oFile = SchoolFile::find($id);
		$filepath = public_path().SchoolFile::$path.$oFile->filename;
		$extension = pathinfo($filepath)['extension'];
		//$outputName = preg_replace('/[^(\x20-\x7F)]*/','', $oFile->description);
		try {
			return Response::download($filepath, Str::ascii($oFile->description) . "." . $extension);
		}catch(\Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException $e){
			return Redirect::back()->with('message', "Wybrany plik nie istnieje. Usuń wpis lub edytuj pozycję.");
		}
	}

	/**
	* Deleteing existing file and removing item form table
	**/
	public function getDelete($id)
	{
		$oFile = SchoolFile::find($id);
		if(SchoolFile::deleteFile($oFile->filename)){
			$oFile->delete();
			return Redirect::route('admin-files')->with('message', 'Plik został usunięty pomyślnie i nie będzie już wyświetlany na stronie.');
		}
		return Redirect::route('admin-files')->with('message', 'Wystąpiły błędy podczas usuwania pliku. Skontaktuj się z administratorem.');
	}


	/**
	* Display form to edit existing file
	**/
	public function getEdit($id)
	{
		$oFile = SchoolFile::find($id);
		$aSpecializations = SchoolSpecialization::all();
		$aFileGroups = Filegroup::all();
		return View::make('admin.file.edit')
			->with('oFile', $oFile)
			->with('aSpecializations', $aSpecializations)
			->with('aFileGroups', $aFileGroups);
	}

	/**
	* Processing editing existing file
	**/
	public function postEdit()
	{
		$validator = Validator::make(Input::all(), SchoolFile::$rulesUpdate);

		if($validator->fails()){
			return Redirect::back()->withErrors($validator);
		}

		$oFile = SchoolFile::find(Input::get('id'));
		$filename = $oFile->filename;

		if(Input::hasFile('file')){
			$filename = SchoolFile::upload(Input::file('file'))->getFilename();
			if(!$filename){
				$messageBag = new MessageBag(array('file' => 'Wystąpił błąd podczas przesyłania pliku. Spróbuj ponownie.'));
				return Redirect::route('admin-editfile')->withErrors($messageBag);
			}
			if(!SchoolFile::deleteFile($oFile->filename)){
				$messageBag = new MessageBag(array('file' => 'Wystąpił błąd podczas usuwania starego pliku. Skontaktuj się z administratorem.'));
				return Redirect::back()->withErrors($messageBag);
			}
		}


		$oFile->id_specialization = Input::get('id_specialization');
		$oFile->id_filegroup = Input::get('id_filegroup') || 0;
		$oFile->description = Input::get('description');
		$oFile->filename = $filename;

		$oFile->save();

		return Redirect::route('admin-files')->with('message', 'Plik został zaktualizowany pomyślnie.');
	}

}