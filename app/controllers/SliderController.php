<?php

class SliderController extends BaseController {

	//--------------------------------------------------
	// BACK-END METHODS
	//--------------------------------------------------


	/**
	* Displaying table with all slides
	**/
	public function indexAdmin()
	{
		$aSlides = Slides::paginate(10);
		return View::make('admin.slides.index')->with('aSlides', $aSlides);
	}


	/**
	* Displaying form to add new slide
	*/
	public function getNew()
	{
		$aSpecializations = SchoolSpecialization::all();
		return View::make('admin.slides.new')->with('aSpecializations', $aSpecializations);
	}

	/**
	* Processing adding new slide
	**/
	public function postNew()
	{
		$validator = Validator::make(Input::all(), array('file' => 'required|max:500', 'slideURL' => 'url'));

		if($validator->fails()){
			return Redirect::route('admin-newslide')->withErrors($validator)->withInput(Input::all());
		}

		$filename = Slides::upload(Input::file('file'));

		if($filename){
			$slide = new Slides();
			$slide->filename = $filename->getFilename();
			$slide->url = Input::get('slideURL');
			
			$slide->save();

			return Redirect::route('admin-slides')->with('message', 'Slajd został dodany pomyślnie');
		}

		$messageBag = new MessageBag(array('file' => 'Wystąpił błąd podczas przesyłania pliku. Spróbuj ponownie.'));
		return Redirect::route('admin-newslide')->withErrors($messageBag)->withInput(Input::all());
		
	}


	/**
	* Get edit form for slide
	*/
	public function getEdit($id)
	{
		$slide = Slides::find($id);
		return View::make('admin.slides.edit')
			->with('slide', $slide);
	}


	/**
	* Process edit slide
	*/
	public function postEdit()
	{
		$validator = Validator::make(Input::all(), array('file' => 'max:500', 'slideURL' => 'url'));

		if($validator->fails()){
			return Redirect::route('admin-editslide')->withErrors($validator)->withInput(Input::all());
		}

		$slide = Slides::find(Input::get('id'));
		$filename = $slide->filename;
		if(Input::hasFile('file')){
			$filename = Slides::upload(Input::file('file'))->getFilename();

			if($filename){
				if(!Slides::deleteFile($slide->filename)){
					return Redirect::route('admin-slides')->with('message', 'Wystąpił błąd podczas usuwania poprzedniego pliku slajdu.');
				}
			}else{
				$messageBag = new MessageBag(array('file' => 'Wystąpił błąd podczas przesyłania pliku. Spróbuj ponownie.'));
				return Redirect::route('admin-newslide')->withErrors($messageBag)->withInput(Input::all());
			}
		}

		$slide->filename = $filename;
		$slide->url = Input::get('slideURL');
		$slide->save();

		return Redirect::route('admin-slides')->with('message', 'Slajd został zaktualizowany pomyślnie');
	}


	/**
	* Process delete slide
	*/
	public function getDelete($id)
	{
		$slide = Slides::find($id);
		if(Slides::deleteFile($slide->filename)){
			$slide->delete();
		}

		return $this->indexAdmin();
	}
}