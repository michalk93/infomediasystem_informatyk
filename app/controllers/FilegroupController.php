<?php

class FilegroupController extends BaseController {

	/**
	* Display list of existing filegroups
	**/
	public function indexAdmin()
	{
		return View::make('admin.filegroup.index')
			->with('aFilegroups', Filegroup::orderBy('id', 'DESC')->paginate(10));
	}

	/**
	* Displaying form to add new filegroup
	**/
	public function getNew()
	{
		return View::make('admin.filegroup.new');
	}

	/**
	* Processing form to add new filegroup to database
	**/
	public function postNew()
	{
		$validator = Validator::make(Input::all(), Filegroup::$rules);

		if($validator->fails()){
			return Redirect::route('admin-newfilegroup')->witErrors($validator)->withInput();
		}

		Filegroup::create(Input::all());

		return Redirect::route('admin-filegroup')->with('message', 'Nowa grupa została dodana pomyślnie.');
	}

	public function getDelete($id){
		$oFilegroup = Filegroup::find($id);
		$aFiles = MyModels\File::where('id_filegroup', $id)->get();
		foreach ($aFiles as $oFile) {
			$oFile->id_filegroup = 0;
			$oFile->save();
		}
		$oFilegroup->delete();

		return Redirect::route('admin-filegroup')->with('message', 'Grupa została usunięta pomyślnie. Pliki należące do grupy zostały przydzielone do grupy "Pozostałe"');
	}
}