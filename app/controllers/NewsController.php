<?php

class NewsController extends BaseController {


	//--------------------------------------------------
	// FRONT-END METHODS
	//--------------------------------------------------

	public function index($id)
	{
		$oNews = News::where('news.id', $id)
				->join('school', 'news.id_school', '=', 'school.id')
				->select('news.*', 'school.name AS school_name')
				->first();

		$aSpecializations	= SchoolSpecialization::where('id_school', $oNews->id_school)->get();

		return View::make('public.news')->with(array('oNews' => $oNews, 'aSpecializations' => $aSpecializations));

	}


	//--------------------------------------------------
	// BACK-END METHODS
	//--------------------------------------------------


	/**
	* Displaying table with all news
	**/
	public function indexAdmin()
	{
		$aNews = News::join('school', 'school.id', '=', 'news.id_school')
			->select('news.*', 'school.name AS school_name')
			->orderBy('created_at', 'DESC')
			->paginate(10);
		return View::make('admin.news.index')->with('aNews', $aNews);
	}

	/**
	* Displaying form to add news
	**/
	public function getNew()
	{
		$aSchools = School::all();
		return View::make('admin.news.new')->with('aSchools', $aSchools);
	}

	/**
	* Processing of adding news to database
	**/
	public function postNew()
	{
		$validator = Validator::make(Input::all(), News::$rules);

		if($validator->fails()){
			return Redirect::route('admin-newnews')->withErrors($validator);
		}

		$inputs = array(
			'content'		=> Input::get('content'),
			'id_school'		=> Input::get('id_school'),
			'created_at'	=> date('Y-m-d')
		);

		News::create($inputs);
		return Redirect::route('admin-news');
	}

	/**
	* Displaying form to edit existing news
	**/
	public function getEdit($id)
	{
		$oNews = News::find($id);
		$aSchools = School::all();
		return View::make('admin.news.edit')->with(array('oNews' => $oNews, 'aSchools' => $aSchools));
	}

	/**
	* Processing of updateing existing news in database
	**/
	public function postEdit($id)
	{
		$validator = Validator::make(Input::all(), News::$rules);

		if($validator->fails()){
			return Redirect::route('admin-editnews', $id)->withErrors($validator)->withInput();
		}

		$oNews = News::find($id);
		$oNews->id_school 	= Input::get('id_school');
		$oNews->content 	= Input::get('content');
		//$oNews->created_at	= date('Y-m-d');
		$oNews->save();

		return Redirect::route('admin-news');
	}


	/**
	* Removing news from database 	
	**/
	public function getDelete($id)
	{
		$oNews = News::find($id);
		$oNews->delete();
		return Redirect::route('admin-news');
	}

}