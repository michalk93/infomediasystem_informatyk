<?php

/**
* Artisan Commands Controller
*/
class ArtisanController extends BaseController
{
	/**
	* Migration start with or withot seeded databse
	*/
	public function migrate($isSeeded = false)
	{
		Artisan::call('migrate');
		if($isSeeded){
			Artisan::call('db:seed');
		}
	}
}