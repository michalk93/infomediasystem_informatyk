<?php

use I4You\File as File;

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
		$aSchools	= School::all();
		$aSlides	= Slides::all();
		return View::make('public.home')
			->with('aSchools', $aSchools)
			->with('aSlides', $aSlides);
	}

	/**
	* 
	*/
	public function showSchool($id)
	{
		$oSchool			= School::where('id', $id)->first();
		$aSpecializations	= SchoolSpecialization::where('id_school', $id)->get();
		$aNews				= News::where('id_school', $id)->orderBy('created_at', 'DESC')->paginate(6);
		foreach($aNews as $oNews){
			$oNews->content = MyValidator::repairHTMLTags(News::ellipsis($oNews->content, 300));
		}
		return View::make('public.school')->with(array('oSchool' => $oSchool, 'aSpecializations' => $aSpecializations, 'aNews' => $aNews));
	}



	public function indexAdmin()
	{
		$aStatistics = array();
		$aStatistics['schools']['count'] = School::count();
		$aStatistics['specializations']['count'] = SchoolSpecialization::count();
		$aStatistics['news']['count'] = News::count();
		$aStatistics['files']['count']	= File::count();
		$aStatistics['files']['size']	= SchoolFile::getFilesSize();
		$aStatistics['files']['groups']['count'] = Filegroup::count();
		foreach(School::all() as $school){
			$aStatistics['schools']['specializations'][$school->name]['count'] = SchoolSpecialization::where('id_school', $school->id)->count();
			$aStatistics['news']['schools'][$school->name]['count'] = News::where('id_school', $school->id)->count();
		}
		
		return View::make('admin.index')
			->with('aStatistics', $aStatistics);
	}

	/**
	* 
	**/
	public function getElearningPlatform()
	{
		return header('Location: http://platforma.informatyk.civ.pl');
	}

}