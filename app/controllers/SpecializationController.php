<?php
use Illuminate\Support\MessageBag as MessageBag;

class SpecializationController extends BaseController {

	//--------------------------------------------------
	// FRONT-END METHODS
	//--------------------------------------------------

	/**
	* Displaying site with specialization's details
	*/
	public function index($id)
	{
		$oSpecialization = DB::table('specialization')
							->where('specialization.id', '=', $id)
							->join('school', 'specialization.id_school', '=', 'school.id')
							->join('specialization_details', 'specialization.id', '=', 'specialization_details.id_specialization')
							->select('specialization.*', 'school.name AS school_name', 'specialization_details.about', 'specialization_details.recruitment', 'specialization_details.teaching_plan')
							->first();
		$aSpecializations = SchoolSpecialization::where('id_school', $oSpecialization->id_school)->get();
		$aFileGroups = SchoolFile::where('id_specialization', $id)->get();
		if(!$aFileGroups->isEmpty()){
			$aFileGroups = null;
			foreach(Filegroup::all() as $oFileGroup){
				$tmpFiles = SchoolFile::where('id_specialization', $id)->where('id_filegroup', $oFileGroup->id)->get();
				if(!$tmpFiles->isEmpty()) {
					$aFileGroups[$oFileGroup->name] = $tmpFiles;
				}
			}

			$aFileGroups['Pozostałe'] = SchoolFile::where('id_specialization', $id)->where('id_filegroup', 0)->get();
		}
		
		
		return View::make('public.specialization')
			->with('oSpecialization', $oSpecialization)
			->with('aSpecializations', $aSpecializations)
			->with('aFileGroups', $aFileGroups);
	}


	//--------------------------------------------------
	// BACK-END METHODS
	//--------------------------------------------------


	/**
	* Displaying list of existing specializations
	**/
	public function indexAdmin()
	{
		return View::make('admin.specialization.index')
			->with('aSpecializations',  SchoolSpecialization::getAllWithDetails());
	}


	/**
	* Displaying form to add new specialization
	**/
	public function getNew()
	{
		$aSchools = School::all();
		return View::make('admin.specialization.new')->with('aSchools', $aSchools);
	}


	/**
	* Processing new specialization 
	**/
	public function postNew()
	{
		$validator = Validator::make(Input::all(), SchoolSpecialization::$rules);

		if($validator->fails()){
			return Redirect::route('admin-newspecialization')->withErrors($validator)->withInput();
		}

		$filename = SchoolSpecialization::uploadImage(Input::file('file'));
		if(!$filename){
			$messageBag = new MessageBag(array('file' => 'Nieprawidłowe wymiary grafiki'));
			return Redirect::route('admin-newspecialization')->withErrors($messageBag)->withInput();
		}

		$inputs = array(
			'name'	=> Input::get('name'),
			'id_school'	=> Input::get('id_school'),
			'filename'	=> $filename
		);

		SchoolSpecialization::create($inputs);

		DB::table('specialization_details')->insert(array(
			'id_specialization'	=> DB::getPdo()->lastInsertId(),
			'about'	=> Input::get('about'),
			'recruitment'	=> Input::get('recruitment'),
			'teaching_plan'	=> Input::get('teaching_plan')
		));

		return $this->indexAdmin();
	}


	/**
	* Displaying form to edit existing specialization
	**/
	public function getEdit($id)
	{
		return View::make('admin.specialization.edit')
			->with('oSpecialization', SchoolSpecialization::getWithDetails($id))
			->with('aSchools', School::all());
	}


	/**
	* Update existing specialization with details
	**/
	public function postEdit()
	{
		if(Input::hasFile('file')){
			$validator = Validator::make(Input::all(), SchoolSpecialization::$updateRules);
		}else {
			$validator = Validator::make(Input::all(), SchoolSpecialization::getRulesExcept(array('file')));
		}

		if($validator->fails()){
			return Redirect::route('admin-editspecialization',Input::get('id'))->withErrors($validator);
		}

		$oSpecialization = SchoolSpecialization::find(Input::get('id'));

		if(Input::hasFile('file')){
			$filename = SchoolSpecialization::uploadImage(Input::file('file'));
			if(!$filename){
				$messageBag = new MessageBag(array('file' => 'Nieprawidłowe wymiary grafiki'));
				return Redirect::route('admin-editspecialization', Input::get('id'))->withErrors($messageBag)->withInput();
			}

			File::delete(public_path().'/img/specializations/'.Input::get('old_filename'));
			$oSpecialization->name = Input::get('name');
			$oSpecialization->id_school = Input::get('id_school');
			$oSpecialization->filename = $filename;

		}else {
			$oSpecialization->name = Input::get('name');
			$oSpecialization->id_school = Input::get('id_school');
			
		}

		$oSpecialization->save();

		DB::table('specialization_details')
			->where('id_specialization', Input::get('id'))
			->update(array(
				'about'	=> Input::get('about'),
				'recruitment'	=> Input::get('recruitment'),
				'teaching_plan'	=> Input::get('teaching_plan')
			));

		return $this->indexAdmin();
	}

	/**
	* Romoving specialization from database
	**/
	public function getDelete($id)
	{
		$oSpecialization = SchoolSpecialization::find($id);
		if(SchoolSpecialization::deleteFile($oSpecialization->filename)){
			$oSpecialization->delete();
		}
		
		return $this->indexAdmin();
	}

}