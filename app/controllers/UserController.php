<?php

class UserController extends BaseController {

	public function getLogin()
	{
		return View::make('admin.login');
	}

	/**
	* Login process for user
	*/
	public function postLogin()
	{
		$email		= Input::get('email', null);
		$password	= Input::get('password', null);
		if(Auth::attempt(array('email' => $email, 'password' => $password), true)){
			return Redirect::route('admin-home');
		}
		return View::make('admin.login')->with('message', User::$messages['login_error']);
	}

	/**
	* Logout from panel adminstration - destroy session
	*/
	public function logout()
	{
		if(Auth::check()){
			Auth::logout();
			Session::flush();
			return Redirect::route('admin-login');
		}
	}

	/**
	* Get form to change password for user
	*/
	public function getChangePassword()
	{
		return View::make('admin.password');
	}

	/**
	* Processing data to change user password
	*/
	public function postChangePassword($id)
	{
		$rules = array(
			'password'				=> 'required|min:5|confirmed',
			'password_confirmation'	=> 'required|min:5'
		);

		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			return Redirect::route('admin-change-password')->withErrors($validator);
		}
		
		$inputs = array('password' => Hash::make(Input::get('password')));
		DB::table('users')->where('id', $id)->update($inputs);
		return Redirect::route('admin-home');
	}
}