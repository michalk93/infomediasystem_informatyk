<?php

class Slides extends I4You\File {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'slides';

	/**
	 * The path where files were stored on server
	 * (related to path returned by public_path() function)
	 *
	 * @var string
	 * @author I4You (MK)
	 */
	public static $path = '/img/slides/';
	public $timestamps = false;
}