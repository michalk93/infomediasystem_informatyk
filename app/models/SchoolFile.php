<?php

class SchoolFile extends I4You\File {
	protected $guarded = array();

	public static $rules = array(
		'id_specialization'	=> 'required|numeric|exists:specialization,id',
		'description' => 'required|between:5,255',
		'file' => 'required|max: 500'
	);

	public static $rulesUpdate = array(
		'id' => 'required|numeric|exists:files,id',
		'id_specialization'	=> 'required|numeric|exists:specialization,id',
		'description' => 'required|between:5,255',
		'file' => 'max: 500'
	);

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'files';

	/**
	 * The path where files were stored on server
	 * (related to path returned by public_path() function)
	 *
	 * @var string
	 * @author I4You (MK)
	 */
	public static $path = '/files/';


}