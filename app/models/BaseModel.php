<?php
/**
* BaseModel to extends for other models
*
* @author I4You (MK)
*
*/

class BaseModel extends Eloquent {

	/**
	* Return table name in database of model
	*
	* @return string
	*/
	public static function getTableName(){
		return with(new static)->getTable();
	}
}