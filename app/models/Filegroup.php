<?php

class Filegroup extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'name'	=> 'required|max:60|unique:filegroup'
	);

	protected $table = "filegroup";
	public $timestamps = false;
}