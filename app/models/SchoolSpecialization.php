<?php

class SchoolSpecialization extends I4You\File {

	protected $guarded = array();

	public static $rules = array(
		'name'	=> 'required|min:10|unique:specialization',
		'id_school'	=> 'required|min:1|exists:school,id',
		'file'	=> 'required|max:100'
	);
	public static $updateRules = array(
		'name'	=> 'required|min:10',
		'id_school'	=> 'required|min:1|exists:school,id',
		'file'	=> 'required|max:100'
	);


	protected $table = "specialization";

	public static $path = "/img/specializations/";

	public $timestamps = false;


	public static function uploadImage($file)
	{
		$image 	= \Image::make($file->getRealPath())->resize(240, 160);
		$image->save();
		return static::upload($file)->getFilename();
	}

	/**
	* Getting specialization from table with details from another related table
	**/
	public static function getWithDetails($id)
	{
		return self::join('specialization_details', 'specialization.id', '=', 'specialization_details.id_specialization')
			->where('specialization.id', $id)
			->first();
	}

	public static function getAllWithDetails()
	{
		return self::join('school', 'school.id', '=', 'specialization.id_school')
			->select('specialization.*', 'school.name AS school_name')
			->orderBy('specialization.id', 'DESC')
			->get();
	}

	/**
	* Get rules array without keys passed as parameter
	**/
	public static function getRulesExcept(array $keys)
	{
		$rules = self::$updateRules;
		foreach($keys as $key){
			unset($rules[$key]);
		}
		return $rules;
	}
}