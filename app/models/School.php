<?php

/**
* Class of school's model
*/
class School extends Eloquent
{
	protected $table 	= "school";
	public $timestamps 	= false;
}