<?php

/**
* Validator class
*/
class MyValidator
{
	//-----------------------------------------
	// VARIABLES
	//-----------------------------------------

	public static $messages = array(
		'file' => array(
			'error'			=> 'Błąd podczas przesyłania pliku',
			'success'		=> 'Plik został przesłany pomyślnie',
			'dimensions'	=> 'Plik ma niepoprawne wymiary'
		),
	);

	//-----------------------------------------
	// METHODS
	//-----------------------------------------
	
	public static function printMessage($errors, $name){
		if($errors->has($name)){
			$label = '<span class="label label-danger">'.$errors->first($name, ':message').'</span>';
			return $label;
		}	
	}

	public static function printErrorClass($errors, $name){
		if($errors->has($name)){
			return 'has-error';
		}
	}

	public static function repairHTMLTags($html){
		// Put all opened tags into an array
		preg_match_all('#<([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
		$openedtags = $result[1];   #put all closed tags into an array
		preg_match_all('#</([a-z]+)>#iU', $html, $result);
		$closedtags = $result[1];
		$len_opened = count($openedtags);
		# Check if all tags are closed
		if (count($closedtags) == $len_opened){
			return $html;
		}
		$openedtags = array_reverse($openedtags);
		# close tags
		for ($i=0; $i < $len_opened; $i++) {
			if (!in_array($openedtags[$i], $closedtags)){
				if($openedtags[$i]!='br'){
					// Ignores <br> tags to avoid unnessary spacing
					// at the end of the string
					$html .= '</'.$openedtags[$i].'>';
				}
			} else {
				unset($closedtags[array_search($openedtags[$i], $closedtags)]);
			}
		}
		return $html;
		}
}