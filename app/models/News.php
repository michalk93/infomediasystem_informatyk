<?php

class News extends Eloquent {
	public $timestamps = false;

	protected $guarded = array();

	public static $rules = array(
		'content'	=> 'required',
		'id_school'	=> 'required|numeric|exists:school,id'
	);

	protected $table = "news";


	/**
	* Splitted date into day, month and year as items of array
	* @return array
	*/
	public static function printDate($date)
	{
		$months = array("STY", "LUT", "MAR", "KWI", "MAJ", "CZE", "LIP", "SIE", "WRZ", "PAŹ", "LIS", "GRU");
		$date = strtotime($date);
		$printDate = array(
			'day'	=> date('d', $date),
			'month'	=> $months[date('n', $date)-1],
			'year'	=> date('Y', $date)
		);

		return $printDate;
	}


	public static function ellipsis($string, $length, $stopanywhere=false) {
	    //truncates a string to a certain char length, stopping on a word if not specified otherwise.
	    if (strlen($string) > $length) {
	        //limit hit!
	        $string = substr($string,0,($length -3));
	        if ($stopanywhere) {
	            //stop anywhere
	            $string .= '...';
	        } else{
	            //stop on a word.
	            $string = substr($string,0,strrpos($string,' ')).'...';
	        }
	    }
	    return $string;
	}

}